# quines

Python code to generate quines. The code is in support of a [blog post](https://drcabana.org/quine/) and is explained there in excruciating detail.
