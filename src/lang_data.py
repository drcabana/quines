babashka = [
    "(let [data [",
    #---- boundary ----#
    " ]",
    " boundary 1",
    " [a b c] [0 boundary (count data)]",
    " quote (char 34) space (char 32) separator (char 32)]",
    " (doseq [d (range 0 boundary)] (println (nth data d)))",
    " (doseq [line data] (println (str space quote line quote separator)))",
    " (doseq [d (range boundary (count data))] (println (nth data d))))"
    ]

fsharp = [
    "let data = [",
    #---- boundary ----#
    "]",
    "let boundary = 1",
    "let a, b, c = 0, boundary, data.Length",
    "let quote, space, separator = string(char 34), string(char 32), string(char 59)",
    "for k in a..b-1 do",
    "   System.Console.WriteLine(data.[k])",
    "for d in data do",
    "   System.Console.WriteLine(space + quote + d + quote + separator)",
    "for k in b..c-1 do",
    "   System.Console.WriteLine(data.[k])"
    ]

# Notice the class name. Java ties the class name to the file name.
# The test machinery wants to call the quine 'quine.java', hence
# the chosen class name.
java = [
    "public class quine {",
    " public static void main(String[] args) {",
    "  String[] data = {",
    #---- boundary ----#
    "  };",
    "  int boundary = 3;",
    "  int a = 0; int b = boundary; int c = data.length;" ,
    "  char quote = (char) 34; char space = (char) 32; char separator = (char) 44;",
    "  for (int k = a; k < b; k++)",
    "    System.out.println(data[k]);",
    "  for (String line : data)",
    "    System.out.println(String.valueOf(space) + quote + line + quote + separator);",
    "  for (int k = b; k < c; k++)",
    "    System.out.println(data[k]);",
    " }",
    "}"
    ]

# Lua uses 1-based indexing; the other languages use 0-based.
lua = [
    "data = {",
    #---- boundary ----#
    "}",
    "boundary = 1",
    "a, b, c = 1, boundary, #data",
    "quote, space, separator = string.char(34), string.char(32), string.char(44)",
    "for k = a, b do",
    "  print(data[k])",
    "end",
    "for k = 1, #data do",
    "  print(space .. quote .. data[k] .. quote .. separator)",
    "end",
    "for k = b+1, c do",
    "  print(data[k])",
    "end"
    ]

python = [
    "data = [",
    #---- boundary ----#
    "]",
    "boundary = 1",
    "a, b, c = 0, boundary, len(data)",
    "quote, space, separator = chr(34), chr(32), chr(44)",
    "for k in range(a,b):",
    "  print(data[k])",
    "for d in data:",
    "  print(space + quote + d + quote + separator)",
    "for k in range(b,c):",
    "  print(data[k])"
    ]

ruby = [
    "data = [",
    #---- boundary ----#
    "]",
    "boundary = 1",
    "a, b, c = 0, boundary, data.length",
    "quote, space, separator = 34.chr, 32.chr, 44.chr",
    "for k in 0..b-1",
    "  puts data[k]",
    "end",
    "data.each { |d| puts space + quote + d + quote + separator}",
    "for k in b..c - 1",
    "  puts data[k]",
    "end"
    ]

facts = { "babashka" :{"boundary":1 , "template":babashka, "separator":' ' },
          "fsharp"   :{"boundary":1 , "template":fsharp  , "separator":';' },
          "java"     :{"boundary":3 , "template":java    , "separator":',' },
          "lua"      :{"boundary":1 , "template":lua     , "separator":',' },
          "python"   :{"boundary":1 , "template":python  , "separator":',' },
          "ruby"     :{"boundary":1 , "template":ruby    , "separator":',' }
        }
supported_langs = facts.keys()

def boundary(lang):
    assert lang in supported_langs
    return facts[lang]["boundary"]

def separator(lang):
    assert lang in supported_langs
    return facts[lang]["separator"]

def template(lang):
    assert lang in supported_langs
    return facts[lang]["template"]